package com.example.mynasaproject.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.mynasaproject.R
import com.example.mynasaproject.ui.notifications.NotificationsFragment

private val TAB_TITLES = arrayOf(
    "Новости",
    "Земля",
    "Марс",
    "Астероиды"
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */

//Класс, распределяющий страницы и кнопки
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        when (position) {
            0 -> {
                return PlaceholderFragment.newInstance(position + 1)
            }
        }
        return MarsFragment.newInstance(position + 1)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }

    override fun getCount(): Int {
        // Show 4 total pages.
        return 4
    }
}