package com.example.mynasaproject.ui.dashboard

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.mynasaproject.ApodData
import com.example.mynasaproject.NewApi
import com.example.mynasaproject.R
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


//Класс фрагмента Dashboard
class DashboardFragment : Fragment() {

    lateinit var list: MutableList<String>
    private var abc = 0
    var x1 = 0.0f
    var x2 = 0.0f
    lateinit var api: NewApi
    var bool = 0

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        list = mutableListOf()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.nasa.gov/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(NewApi::class.java)

        updateSome(root)
        root.setOnTouchListener { v, event ->
            if (!root.switcch.isChecked) {
                if (event.action == MotionEvent.ACTION_DOWN) {
                    x1 = event.x
                } else if (event.action == MotionEvent.ACTION_UP) {
                    x2 = event.x
                    if (x1 > x2 && x1 - x2 > 50) {
                        if (bool == 0) {
                            root.fir.animate()
                                .rotation(-30f)
                                .translationX(-1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else if (bool == 1) {
                            root.sec.animate()
                                .rotation(-30f)
                                .translationX(-1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else {
                            root.thi.animate()
                                .rotation(-30f)
                                .translationX(-1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        }
                    } else if (x2 - x1 > 50) {
                        if (bool == 0) {
                            root.fir.animate()
                                .rotation(30f)
                                .translationX(1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else if (bool == 1) {
                            root.sec.animate()
                                .rotation(30f)
                                .translationX(1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else {
                            root.thi.animate()
                                .rotation(30f)
                                .translationX(1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        }
                    }
                }
            } else {
                val display: Display = (root.context as AppCompatActivity).getWindowManager().getDefaultDisplay()
                val metricsB = DisplayMetrics()
                display.getMetrics(metricsB)
                if (event.action == MotionEvent.ACTION_MOVE) {
                    x1 = event.x
                    if (bool == 0) {
                        root.fir.animate()
                            .rotation(40*(x1-metricsB.widthPixels/2)/metricsB.widthPixels)
                            .translationX(x1-metricsB.widthPixels/2)
                            .translationY(-(x1-metricsB.widthPixels/2)/7)
                            .setDuration(0)
                            .start()
                    } else if (bool == 1) {
                        root.sec.animate()
                            .rotation(40*(x1-metricsB.widthPixels/2)/metricsB.widthPixels)
                            .translationX(x1-metricsB.widthPixels/2)
                            .translationY(-(x1-metricsB.widthPixels/2)/7)
                            .setDuration(0)
                            .start()
                    } else {
                        root.thi.animate()
                            .rotation(40*(x1-metricsB.widthPixels/2)/metricsB.widthPixels)
                            .translationX(x1-metricsB.widthPixels/2)
                            .translationY(-(x1-metricsB.widthPixels/2)/7)
                            .setDuration(0)
                            .start()
                    }
                } else if (event.action == MotionEvent.ACTION_UP) {
                    if (x1 > metricsB.widthPixels/6*5) {
                        if (bool == 0) {
                            root.fir.animate()
                                .rotation(30f)
                                .translationXBy(x1)
                                .translationX(1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else if (bool == 1) {
                            root.sec.animate()
                                .rotation(-30f)
                                .translationXBy(x1)
                                .translationX(1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else {
                            root.thi.animate()
                                .rotation(-30f)
                                .translationXBy(x1)
                                .translationX(1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        }
                    } else if (x1 < metricsB.widthPixels/6) {
                        if (bool == 0) {
                            root.fir.animate()
                                .rotation(-30f)
                                .translationXBy(x1)
                                .translationX(x1-1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else if (bool == 1) {
                            root.sec.animate()
                                .rotation(-30f)
                                .translationXBy(x1)
                                .translationX(x1-1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        } else {
                            root.thi.animate()
                                .rotation(-30f)
                                .translationXBy(x1)
                                .translationX(x1-1000f)
                                .translationY(-150f)
                                .setDuration(400)
                                .withEndAction {
                                    updatePictures(root)
                                }
                                .start()
                        }
                    } else {
                        if (bool == 0) {
                            root.fir.animate()
                                .rotation(0f)
                                .translationX(0f)
                                .translationY(0f)
                                .setDuration(50)
                                .start()
                        } else if (bool == 1) {
                            root.sec.animate()
                                .rotation(0f)
                                .translationX(0f)
                                .translationY(0f)
                                .setDuration(50)
                                .start()
                        } else {
                            root.thi.animate()
                                .rotation(0f)
                                .translationX(0f)
                                .translationY(0f)
                                .setDuration(50)
                                .start()
                        }
                    }
                }
            }
            if (abc >= 363) {
                updateSome(root)
            }
            return@setOnTouchListener true
        }

        return root
    }

    fun updateSome(root: View) {
        for (i in 1..365) {
            list.add("2020-${(1..12).random()}-${(1..28).random()}")
        }
        abc=0
        updatePictures(root)
    }

    fun updatePictures(root: View) {
        if (abc == 0) {
            api.apod(list[abc]).enqueue(object : Callback<ApodData> {
                override fun onFailure(call: Call<ApodData>, t: Throwable) {
                    Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<ApodData>, response: Response<ApodData>) {
                    if (response.isSuccessful) {
                        Glide.with(root.context)
                            .load(response.body()!!.url)
                            .into(root.first)
                        root.name.text = response.body()!!.title
                        root.des.text = response.body()!!.explanation
                    } else {
                        Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }
            })

            api.apod(list[abc + 1]).enqueue(object : Callback<ApodData> {
                override fun onFailure(call: Call<ApodData>, t: Throwable) {
                    Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<ApodData>, response: Response<ApodData>) {
                    if (response.isSuccessful) {
                        Glide.with(root.context)
                            .load(response.body()!!.url)
                            .into(root.second)
                        root.name1.text = response.body()!!.title
                        root.des1.text = response.body()!!.explanation
                    } else {
                        Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }
            })
            api.apod(list[abc + 2]).enqueue(object : Callback<ApodData> {
                override fun onFailure(call: Call<ApodData>, t: Throwable) {
                    Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<ApodData>, response: Response<ApodData>) {
                    if (response.isSuccessful) {
                        Glide.with(root.context)
                            .load(response.body()!!.url)
                            .into(root.third)
                        root.name2.text = response.body()!!.title
                        root.des2.text = response.body()!!.explanation
                        root.fir.visibility = View.VISIBLE
                        root.sec.visibility = View.VISIBLE
                        root.thi.visibility = View.VISIBLE
                        root.progressBar.visibility = View.INVISIBLE
                        root.fir.elevation = 2f
                        root.sec.elevation = 1f
                    } else {
                        Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }
            })
        } else {
                if (bool==0) {
                    root.fir.animate()
                        .rotation(0f)
                        .translationX(0f)
                        .translationY(0f)
                        .setDuration(0)
                        .start()
                    api.apod(list[abc+2]).enqueue(object : Callback<ApodData> {
                        override fun onFailure(call: Call<ApodData>, t: Throwable) {
                            Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
                        }

                        override fun onResponse(call: Call<ApodData>, response: Response<ApodData>) {
                            if (response.isSuccessful) {
                                Glide.with(root.context)
                                    .asBitmap()
                                    .load(response.body()!!.url)
                                    .into(root.first)
                                root.name.text = response.body()!!.title
                                root.des.text = response.body()!!.explanation
                            } else {
                                Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                            }
                        }
                    })
                    root.fir.elevation=0f
                    root.sec.elevation=2f
                    root.thi.elevation=1f
                    bool = 1
                } else if (bool == 1) {
                    root.sec.animate()
                        .rotation(0f)
                        .translationX(0f)
                        .translationY(0f)
                        .setDuration(0)
                        .start()
                    api.apod(list[abc + 2]).enqueue(object : Callback<ApodData> {
                        override fun onFailure(call: Call<ApodData>, t: Throwable) {
                            Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
                        }

                        override fun onResponse(call: Call<ApodData>, response: Response<ApodData>) {
                            if (response.isSuccessful) {
                                Glide.with(root.context)
                                    .asBitmap()
                                    .load(response.body()!!.url)
                                    .into(root.second)
                                root.name1.text = response.body()!!.title
                                root.des1.text = response.body()!!.explanation
                            } else {
                                Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                            }
                        }
                    })
                    root.fir.elevation=1f
                    root.sec.elevation=0f
                    root.thi.elevation=2f
                    bool = 2
                } else {
                    root.thi.animate()
                        .rotation(0f)
                        .translationX(0f)
                        .translationY(0f)
                        .setDuration(0)
                        .start()
                    api.apod(list[abc + 2]).enqueue(object : Callback<ApodData> {
                        override fun onFailure(call: Call<ApodData>, t: Throwable) {
                            Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
                        }

                        override fun onResponse(call: Call<ApodData>, response: Response<ApodData>) {
                            if (response.isSuccessful) {
                                Glide.with(root.context)
                                    .asBitmap()
                                    .load(response.body()!!.url)
                                    .into(root.third)
                                root.name2.text = response.body()!!.title
                                root.des2.text = response.body()!!.explanation
                            } else {
                                Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                            }
                        }
                    })
                    root.fir.elevation=2f
                    root.sec.elevation=1f
                    root.thi.elevation=0f
                    bool = 0
                }
        }
        abc++
    }
}