package com.example.mynasaproject.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.mynasaproject.AdapterForPhotosRecyclerView
import com.example.mynasaproject.ApodData
import com.example.mynasaproject.NewApi
import com.example.mynasaproject.R
import com.example.mynasaproject.sampledata.NearData
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.fragment_news.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */

//Фрагмент с новостями
class PlaceholderFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_news, container, false)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.nasa.gov/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(NewApi::class.java)

        val list = mutableListOf<ApodData>()

        function(0, api, root, list)

        api.near("2020-12-27").enqueue(object : Callback<NearData>{
            override fun onFailure(call: Call<NearData>, t: Throwable) {
                Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<NearData>, response: Response<NearData>) {
                if (response.isSuccessful) {
                    Toast.makeText(root.context, response!!.body()!!.near_earth_objects["2020-12-27"]!![0]!!.name.toString(), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                }
            }

        })

        return root
    }

    fun function(i: Int, api: NewApi, root: View, list: MutableList<ApodData>) {
        var date = Date()
        val formatter = SimpleDateFormat("yyyy")
        val formatter2 = SimpleDateFormat("MM")
        val formatter3 = SimpleDateFormat("dd")
        var day = formatter3.format(date).toInt()
        var mount = formatter2.format(date).toInt()
        var year = formatter.format(date).toInt()

        day-=i

        val answer =
            "$year-$mount-$day"

        api.apod(answer).enqueue(object : Callback<ApodData> {
            override fun onFailure(call: Call<ApodData>, t: Throwable) {
                Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<ApodData>, response: Response<ApodData>) {
                if (response.isSuccessful) {
                    list.add(response.body()!!)
                    if (i==7) {
                        val adapter = AdapterForPhotosRecyclerView()

                        adapter.setValues(list, root.context)

                        root.recyclerView.adapter = adapter
                    } else {
                        function(i + 1, api, root, list)
                    }
                } else {
                    Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}