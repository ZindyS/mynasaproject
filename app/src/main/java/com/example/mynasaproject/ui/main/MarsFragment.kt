package com.example.mynasaproject.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.mynasaproject.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * A placeholder fragment containing a simple view.
 */

//Класс со статьями про Марс, Землю и Астероиды
class MarsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_search, container, false)

        //Подготовка запроса
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.nasa.gov/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(NewApi::class.java)
        var str = "Asteroid"

        //Проверка, на какую именно кнопку нажал пользователь
        if (sectionNumber1==2) {
            str = "Earth"
        } else if (sectionNumber1 == 3) {
            str = "Mars"
        }

        //Запрос на сервер
        api.mars(str).enqueue(object: Callback<SearchData> {
            //При неудаче
            override fun onFailure(call: Call<SearchData>, t: Throwable) {
                Toast.makeText(root.context, t.message, Toast.LENGTH_SHORT).show()
            }

            //Если всё хорошо
            override fun onResponse(call: Call<SearchData>, response: Response<SearchData>) {
                if (response.isSuccessful) {
                    val adapter = AdapterForSearchRecyclerView()
                    adapter.setValues(response.body()!!, root.context)

                    root.sear.adapter = adapter
                } else {
                    Toast.makeText(root.context, response.message(), Toast.LENGTH_SHORT).show()
                }
            }

        })

        return root
    }

    companion object {
        var sectionNumber1 = 1
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): MarsFragment {
            sectionNumber1
            sectionNumber1 = sectionNumber
            return MarsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}