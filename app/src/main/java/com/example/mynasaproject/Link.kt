package com.example.mynasaproject

data class Link(
    val href: String,
    val rel: String,
    val render: String,
    val prompt: String
)