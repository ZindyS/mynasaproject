package com.example.mynasaproject

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

//Класс адаптера для списка постов про Землю, Марс и Астероиды
class AdapterForSearchRecyclerView: RecyclerView.Adapter<AdapterForSearchRecyclerView.VH>() {

    private lateinit var list: SearchData
    private lateinit var context: Context

    //Кастомная функция для получения результатов
    fun setValues(list: SearchData, context: Context) {
        this.list = list
        this.context = context
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.name)
        val descr = itemView.findViewById<TextView>(R.id.descripteon)
        val image = itemView.findViewById<ImageView>(R.id.image)
        val date = itemView.findViewById<TextView>(R.id.date)
        val author = itemView.findViewById<TextView>(R.id.author)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.search_item, parent, false))
    }

    override fun getItemCount(): Int {
        return list.collection.items.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.name.text = list.collection.items[position].data[0].title
        holder.descr.text = list.collection.items[position].data[0].description
        holder.date.text = list.collection.items[position].data[0].date_created
        holder.author.text = list.collection.items[position].data[0].photographer
        Glide.with(context)
            .load(list.collection.items[position].links[0].href)
            .into(holder.image)
    }
}