package com.example.mynasaproject

data class Item(
    val `data`: List<Data>,
    val href: String,
    val links: List<Link>
)