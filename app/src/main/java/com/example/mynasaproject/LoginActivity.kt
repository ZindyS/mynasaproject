package com.example.mynasaproject

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*

//Активити с авторизацией
class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var sh: SharedPreferences
    private lateinit var ed: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = Firebase.auth
        sh = getSharedPreferences("NASA", 0)
        ed = sh.edit()

        if (sh.getString("password", "") != "") {
            goToMainActivity()
        }
    }

    //При нажатии на кнопку Далее
    fun onSignInCl(v: View) {
        //Проверка на пустоту полей
        if (username.text.isNotEmpty() && password.text.isNotEmpty()) {
            //Авторизация
            auth.signInAnonymously()
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val user = auth.currentUser
                        //Вызов кастомного метода (он ниже)
                        updateUI(user)
                    } else {
                        Toast.makeText(baseContext, "Ошибка авторизации", Toast.LENGTH_SHORT).show()
                        updateUI(null)
                    }
                }
        } else {
            Toast.makeText(baseContext, "Проверьте поля!", Toast.LENGTH_SHORT).show()
        }
    }

    //При нажатии на кнопку Пропустить
    fun onCancelCl(v: View) {
        //Вызов кастомного метода
        goToMainActivity()
    }

    //Метод для перехода на след активити
    fun goToMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    //Кастомный метода для авторизации
    fun updateUI(user: FirebaseUser?) {
        //Ещё одна проверка
        if (user != null) {
            //Запрос на сервер
            val ref = FirebaseDatabase.getInstance().reference
            ref.addListenerForSingleValueEvent(object: ValueEventListener {
                //Если не получили ответ
                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(baseContext, "Ошибка при получении данных!", Toast.LENGTH_SHORT).show()
                }
                //Если получили ответ
                override fun onDataChange(snapshot: DataSnapshot) {
                    //Проверка на то, совпадает ли пароль, введёный пользователем с паролем в базе данных
                    if (snapshot.child("response").child("users_list").child(username.text.toString()).child("password").value == password.text.toString()) {
                        ed.putString("password", password.text.toString())
                        ed.commit()
                        goToMainActivity()
                    } else {
                        Toast.makeText(baseContext, "Неправильный логин или пароль!", Toast.LENGTH_SHORT).show()
                    }
                }
            })
        }
    }
}