package com.example.mynasaproject

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

//Класс адаптера для новостей
class AdapterForPhotosRecyclerView: RecyclerView.Adapter<AdapterForPhotosRecyclerView.VH>() {

    private lateinit var list: MutableList<ApodData>
    private lateinit var context: Context

    //Кастомный метод для получения значений
    fun setValues(list: MutableList<ApodData>, context: Context) {
        this.list = list
        this.context = context
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.name)
        val descr = itemView.findViewById<TextView>(R.id.descripteon)
        val image = itemView.findViewById<ImageView>(R.id.image)
        val date = itemView.findViewById<TextView>(R.id.date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.name.text = list[position].title
        holder.descr.text = list[position].explanation
        holder.date.text = list[position].date
        Glide.with(context)
            .load(list[position].url)
            .into(holder.image)
    }
}