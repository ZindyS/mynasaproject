package com.example.mynasaproject.sampledata

data class Links(
    val next: String,
    val prev: String,
    val self: String
)