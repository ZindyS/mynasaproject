package com.example.mynasaproject.sampledata

data class Feet(
    val estimated_diameter_max: Double,
    val estimated_diameter_min: Double
)