package com.example.mynasaproject.sampledata

data class NearData(
    val element_count: Int,
    val links: Links,
    val near_earth_objects: Map<String, List<X20201215>>
)