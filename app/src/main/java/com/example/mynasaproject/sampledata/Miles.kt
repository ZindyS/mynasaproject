package com.example.mynasaproject.sampledata

data class Miles(
    val estimated_diameter_max: Double,
    val estimated_diameter_min: Double
)