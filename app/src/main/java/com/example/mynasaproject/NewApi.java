package com.example.mynasaproject;

import com.example.mynasaproject.sampledata.NearData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

//Класс описания Url'ов для запросов
public interface NewApi {
    @GET("https://api.nasa.gov/planetary/apod?api_key=afxbr4vwr2BVC7tNvEyVRFflPyBl3T1Lli5drorf")
    Call<ApodData> apod(@Query("date") String date);

    @GET("https://images-api.nasa.gov/search")
    Call<SearchData> mars(@Query("q") String q);

    @GET("https://api.nasa.gov/neo/rest/v1/feed?api_key=TwXcGEBDhnccH0HkvSC6N39sc1PEWVBhrsrn9hTc")
    Call<NearData> near(@Query("start_data") String data);
}
